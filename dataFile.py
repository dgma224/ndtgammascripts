#create a dataFile class
#this will hopefully be compatible with the Nab datafile class
#that way I can use them together

import numpy as np
import os
import matplotlib.pyplot as plt


class dataFile:
	def __init__(self, fileName):
		#name and open the file based on the input
		self.fileName = fileName
		self.__file = open(self.fileName, "rb")
		#define the data types
		self.wavelength = 500000
		self.__waveType = str(self.wavelength)+'h'
		#now load in the file
		#figure out how many "waveforms" are in the file
		self.getFileReadInformation()	
		#load all of these
		self.loadFile()	

	def getFileReadInformation(self):
		size = os.stat(self.fileName).st_size
		self.numEvents = int(int(size/(self.wavelength * 2 * 8 + 4 * 5))/2)
	
	def loadFile(self):
		self.__origFile = np.fromfile(self.__file, dtype=self.__headWavesType(self.wavelength), count = -1)
	
	
	#when these return, they return a copy of the values
	#no function is allowed to modify the original information
	def waves(self):
		return self.__waves[:]

	def heads(self):
		return self.__heads[:]

	def origFile(self):
		return self.__origFile[:]

	#this returns all the waveforms, with headers, for a particular pixel
	def getPixel(self, pixel):
		return self.__origFile['waves']['wave'+str(pixel)][:]

	def __headerType(self):
		return {'names': ['word1', 'word2', 'word3', 'word4', 'word5'], 'formats': ['uint32', 'uint32', 'uint32', 'uint32','uint32']}

	def __wavesType(self, length):
		names = []
		formats = []
		for i in range(16):
			names.append('wave'+str(i))
			formats.append(str(length)+'H')
		return {'names': names, 'formats': formats}

	def __headWavesType(self, wavelength):
		return {'names': ['header0', 'header1', 'waves'], 'formats': [self.__headerType(), self.__headerType(), self.__wavesType(wavelength)]}
    

    
'''
This class handles the reading of result files
the format is simple. Each waveforms results have a header and the results

Header:
    Event
    Pixel
    DAQ Timestamp
    Number of Peaks (N)
    
Results:
    N tuples, first element is the timestamp, second is the energy

'''

class resultFile:
    def __init(self, filename):
        #this opens the file and reads in the results
        self.filename = filename
        
        
    def writeResults(self, filename):
        return

        
    




